﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.DTOs
{
    public class CommentDto
    {
        
        public int BookId { get; set; }
        public string Nick { get; set; }
        public string Text { get; set; }

    }
}