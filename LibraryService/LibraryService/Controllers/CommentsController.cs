﻿using LibraryService.DTOs;
using LibraryService.interfaces;
using LibraryService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LibraryService.Controllers
{
    public class CommentsController : ApiController
    {
        ICommentRepository _repository;
        public CommentsController(ICommentRepository repo)
        {
            _repository = repo;
        }
        public IHttpActionResult PostComm(CommentDto comment)
        {
            return Ok(_repository.Add(comment));
        }
        [Route("api/comm/{bookId}")]
        public IEnumerable<Comment> Get(int bookId)
        {
            return _repository.GetComments(bookId);
        }
    }
}
