﻿using LibraryService.interfaces;
using LibraryService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LibraryService.Controllers
{
    public class GenresController : ApiController
    {
        IGenreRepository _repo;
        public GenresController(IGenreRepository repo)
        {
            _repo = repo;
        }
        public IEnumerable<Genre> Get()
        {
            return _repo.GetAll();
        }
    }
}
