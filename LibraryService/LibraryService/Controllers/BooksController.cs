﻿using LibraryService.interfaces;
using LibraryService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LibraryService.Controllers
{
    public class BooksController : ApiController
    {

        IBookRepository _repo;
        public BooksController(IBookRepository repo)
        {
            _repo = repo;
        }
        [Route("api/books")]
        public IEnumerable<Book> Get()
        {
            return _repo.GetAll();
        }
        [Route("api/book/{id}")]
        public IHttpActionResult Get(int id)
        {
            var book = _repo.GetById(id);
            if (book == null)
            {
                return NotFound();
            }
            return Ok(book);
        }
        [Route("api/recommended")]
        public IEnumerable<Book> GetRecommended()
        {
            return _repo.Recomended();
        }
        [Route("api/search")]
        public IEnumerable<Book> PostSearch(Search search)
        {
            return _repo.Search(search);
        }
    }
}
