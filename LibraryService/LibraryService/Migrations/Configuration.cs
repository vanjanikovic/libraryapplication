namespace LibraryService.Migrations
{
    using LibraryService.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<LibraryService.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(LibraryService.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            Genre g1 = new Genre() { Id = 1, Name = "Thriller" };
            Genre g2 = new Genre() { Id = 2, Name = "Romance" };
            Genre g3 = new Genre() { Id = 3, Name = "Mystery" };
            Genre g4 = new Genre() { Id = 4, Name = "Horror" };
            Genre g5 = new Genre() { Id = 5, Name = "Biography" };
            Genre g6 = new Genre() { Id = 6, Name = "Domestic fiction" };

            context.Genres.AddOrUpdate(g1, g2, g3, g4, g5,g6);
            context.SaveChanges();

            Book b1 = new Book() { Id = 1, Name = "The Da Vinci Code", Available = true,
                Author = "Dan Brown",
                Description = "The Da Vinci Code follows 'symbologist'" +
                " Robert Langdon and cryptologist Sophie Neveu after a murder" +
                " in the Louvre Museum in Paris causes them to become involved in a" +
                " battle between the Priory of Sion and Opus Dei over the possibility of Jesus Christ " +
                "and Mary Magdalene having had a child together.",
                IsRecommended = true
            };
            Book b2 = new Book()
            {
                Id = 2,
                Name = "Angels and Demons",
                Available = false,
                Author = "Dan Brown",
                Description = "World-renowned Harvard symbologist Robert Langdon is" +
                " summoned to a Swiss research facility to analyze a cryptic symbol seared into the" +
                " chest of a murdered physicist. What he discovers is unimaginable: a deadly vendetta" +
                " against the Catholic Church by a centuries-old underground organization -- the Illuminati.",
                IsRecommended = true
            };
            Book b3 = new Book()
            {
                Id = 3,
                Author = "Stephen King",
                Name = "if it bleeds",
                Available = true,
                IsRecommended = true,
                Description = "Holly Gibney of the Finders Keepers detective agency is working on the" +
                " case of a missing dog - and on her own need to be more assertive - when " +
                "she sees the footage on TV. But when she tunes in again," +
                " to the late - night " +
                 "report," +
                 " she realises there is something not quite right about the correspondent " +
                    "who was first on the scene. So begins 'If It Bleeds'," +
                 " a stand - alone sequel to the " +
                "No. 1 bestselling The Outsider featuring the incomparable Holly on her first solo case -" +
                "and also the riveting title story in Stephen King's brilliant new collection."
            };
            Book b4 = new Book()
            {
                Id = 4,
                Author = "Stephen King",
                Name="Outsider",
                IsRecommended = false,
                Available = true,
                Description = "Detective Anderson sets out to investigate the impossible: " +
            "how can the suspect have been both at the scene of the crime and in another town?"
            };
            Book b5=new Book() { 
            Id=5,Author= " Khaled Hosseini",
            IsRecommended=false,
            Available=true,
            Name="A Thousand Splendid Suns",
            Description= "Mariam is only fifteen when she is sent to Kabul to marry Rasheed." +
            " Nearly two decades later, a friendship grows between Mariam and a local teenager, Laila, " +
            "as strong as the ties between mother and daughter. When the Taliban take over, life becomes a" +
            " desperate struggle against starvation, brutality and fear. Yet love can move a person to act in" +
            " unexpected ways, and lead them to overcome the most daunting obstacles with a startling heroism."
            };
            Book b6 = new Book() {
                Id=6,
                Author= "Elizabeth Gilbert",
                Name="Eat Pray Love",
                Available=false,
                IsRecommended=false,
                Description= "It's 3 a.m. and Elizabeth Gilbert is sobbing on the bathroom floor." +
                " She's in her thirties, she has a husband, a house, they're trying for a baby - and she" +
                " doesn't want any of it. A bitter divorce and a turbulent love affair later, she emerges" +
                " battered and bewildered and realises it is time to pursue her own journey in search of three " +
                "things she has been missing: pleasure, devotion and balance. So she travels to Rome, where she" +
                " learns Italian from handsome, brown-eyed identical twins and gains twenty-five pounds, an ashram " +
                "in India, where she finds that enlightenment entails getting up in the middle of the night to scrub" +
                " the temple floor, and Bali where a toothless medicine man of indeterminate age offers her a " +
                "new path to peace: simply sit still and smile. And slowly happiness begins to creep up on her."
            };
            context.Books.AddOrUpdate(b1, b2,b3,b4,b5,b6);
            context.SaveChanges();

            BookGenre bg1 = new BookGenre() { BookId = 1, GenreId = 1 };
            BookGenre bg2 = new BookGenre() { BookId = 1, GenreId = 3 };
            BookGenre bg3 = new BookGenre() { BookId = 2, GenreId = 1 };
            BookGenre bg4 = new BookGenre() { BookId = 2, GenreId = 3 };
            BookGenre bg5 = new BookGenre() { BookId = 3, GenreId = 1 };
            BookGenre bg6 = new BookGenre() { BookId = 3, GenreId = 4 };
            BookGenre bg7 = new BookGenre() { BookId = 4, GenreId = 1 };
            BookGenre bg8 = new BookGenre() { BookId = 4, GenreId = 3 };
            BookGenre bg9 = new BookGenre() { BookId = 4, GenreId = 4 };
            BookGenre bg10 = new BookGenre() { BookId = 5, GenreId = 6 };
            BookGenre bg11 = new BookGenre() { BookId = 6, GenreId = 2 };
            BookGenre bg12 = new BookGenre() { BookId = 6, GenreId = 5 };

            context.BookGenre.AddOrUpdate(bg1, bg2, bg3, bg4, bg5, bg6, bg7, bg8, bg9, bg10, bg11, bg12);
            context.SaveChanges();

        }
    }
}
