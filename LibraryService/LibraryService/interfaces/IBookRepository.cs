﻿using LibraryService.DTOs;
using LibraryService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryService.interfaces
{
    public interface IBookRepository
    {
        IEnumerable<Book> GetAll();
        BookDto GetById(int id);
        IEnumerable<Book> Recomended();
        IEnumerable<Book> Search(Search search);
    }
}
