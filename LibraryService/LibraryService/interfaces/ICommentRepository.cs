﻿using LibraryService.DTOs;
using LibraryService.Migrations;
using LibraryService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryService.interfaces
{
    public interface ICommentRepository
    {
        IEnumerable<Comment> GetComments(int bookId);
        int Add(CommentDto comment);
    }
}
