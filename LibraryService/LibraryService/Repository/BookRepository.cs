﻿using LibraryService.DTOs;
using LibraryService.interfaces;
using LibraryService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.Repository
{
    public class BookRepository : IBookRepository, IDisposable
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public IEnumerable<Book> GetAll()
        {
            return db.Books;
        }


        public IEnumerable<Book> Recomended()
        {
            return db.Books.Where(z => z.IsRecommended);
        }

       public BookDto GetById(int id)
        {
            //var genres = db.BookGenre.Include("Genre").GroupBy(x => x.BookId).FirstOrDefault(
            //    x=>x.Key==id).Select(x=>new Genre() {Id=x.Genre.Id,Name=x.Genre.Name }).ToList(); 
            var book = db.Books.FirstOrDefault(x => x.Id == id);
            if(book==null)
            {
                return null;
            }
            BookDto details = new BookDto()
            {
                Id = book.Id,
                Name = book.Name,
                Author = book.Author,
                IsRecommended = book.IsRecommended,
                Available = book.Available,
                Description = book.Description,
                Genres = new List<Genre>()
            };
            return details;

        }

        public IEnumerable<Book> Search(Search search)
        {
            var books = db.Books.AsQueryable();
            
            if(search.GenreId!=0)
            {
                books = books;
            }
            if(search.Name!=null)
            {
                books = books.Where(x => x.Name.Contains(search.Name));
            }
            if (search.Author != null)
            {
                books = books.Where(x => x.Name.Contains(search.Author));
            }
            return books;
        }
    }
}