﻿using LibraryService.DTOs;
using LibraryService.interfaces;
using LibraryService.Migrations;
using LibraryService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.Repository
{
    public class CommentRepository:ICommentRepository,IDisposable
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Comment> GetComments(int bookId)
        {
            return db.Comments.Where(x => x.BookId == bookId);
        }

        public int Add(CommentDto comment)
        {
            var newComment = new Comment() { 
            Nick=comment.Nick,
            Text=comment.Text,
            BookId=comment.BookId
            };
            db.Comments.Add(newComment);
            db.SaveChanges();
            return comment.BookId;
        }
    }
}