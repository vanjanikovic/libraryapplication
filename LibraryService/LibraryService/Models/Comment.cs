﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Nick { get; set; }
        public int? BookId { get; set; }
        public Book Book { get; set; }
    }
}