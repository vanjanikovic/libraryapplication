﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.Models
{
    public class Search
    {
        public int GenreId { get; set; }
        public string Author { get; set; }
        public string Name { get; set; }
    }
}